
'use strict';

$(document).ready(function(){
  $('table tr').on('click', function(e) {
    $('table tr').removeClass('marked');
    $(this).addClass('marked');
  });
});

$('#myTableId').DataTable({
  buttons: [ 'excel', 'pdf', 'copy' ]
});
$('#save').click(function () {
    alert($('#mytable').find('input[type="checkbox"]:checked').length + ' checked');
});

$(function () {
 var listInput = [];
  $("#btnexcellSelect").attr("disabled", "disabled");
  
  $('input[type=checkbox]').change(function() {
  listInput = [];
      $("#tester > tbody  tr").each(function () {      			
      		if ($('input[type=checkbox]').is(':checked')) {  
             $('#btnexcellSelect').removeAttr('disabled');            
          }else{         
          	 $("#btnexcellSelect").attr("disabled", "disabled");
             $('#result').html('');
             
          }
      });
  });

  $('#btnexcellSelect').click(function (e) {
    if ($('input[type=checkbox]').is(':checked')) {
     	$('input[type=checkbox]').each(function(){
      		if ($(this).is(':checked')) 
          {
            listInput.push($(this).attr('data-value'));
          }
      });
      $('#result').html(listInput.join(", "));
    }
    e.preventDefault();
  });
});
